#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image.h"

struct image rotate_counterclockwise(const struct image* img);

#endif //IMAGE_TRANSFORMER_ROTATE_H

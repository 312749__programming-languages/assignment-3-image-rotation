#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/* Question to the teacher:
 *  Should struct definitions be placed in header files or in implementation files?
 *  In what cases it's better to use first approach (struct definitions inside header),
 *  and in what cases - the second (functions as API with only declaring structs)?
 * */
struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

bool is_image_valid(const struct image* img);

struct image create_image(uint64_t img_width, uint64_t img_height);
void delete_image(struct image* img);

#endif //IMAGE_TRANSFORMER_IMAGE_H

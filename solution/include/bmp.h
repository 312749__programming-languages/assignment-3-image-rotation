#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

#define BMP_PADDING_SIZE        4

#define BMP_FILE_TYPE           19778
#define BMP_RESERVED            0
#define BMP_OFFSET_BITS         54
#define BMP_INFO_SIZE           40
#define BMP_PLANES              1
#define BMP_BITS_PER_PIXEL      24
#define BMP_COMPRESSION         0
#define BMP_SIZE_IMAGE          0
#define BMP_X_PIXELS_PER_METER  0
#define BMP_Y_PIXELS_PER_METER  0
#define BMP_COLORS_USED         0
#define BMP_COLORS_IMPORTANT    0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* out, const struct image* img);

#endif //IMAGE_TRANSFORMER_BMP_H

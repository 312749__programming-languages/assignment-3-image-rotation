#include "../include/image.h"

bool is_image_valid(const struct image* img) {
    if (img->height <= 0 || img->width <= 0)
        return false;
    if (img->data == NULL)
        return false;

    return true;
}

struct image create_image(uint64_t width, uint64_t height) {
    struct pixel* data = (struct pixel*) malloc(
        sizeof(struct pixel) * width * height
    );

    return (struct image) {
        .width  = width,
        .height = height,
        .data   = data
    };
}

void delete_image(struct image* img) {
    if (img) {
        free(img->data);
        img->data = NULL;
    }
}

#include "../include/bmp.h"

static uint8_t calculate_padding(const uint64_t img_width) {
    return (BMP_PADDING_SIZE - sizeof(struct pixel) * img_width) % BMP_PADDING_SIZE;
}

static size_t get_content_size(const struct image* img) {
    return (img->width * sizeof(struct pixel)) * img->height;
}

static size_t get_file_size(const struct image* img) {
    return sizeof(struct bmp_header) + get_content_size(img);
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;

    size_t read_header_repeats = 1;
    size_t read_header_res = fread(
        &header,
        sizeof(struct bmp_header),
        read_header_repeats,
        in
    );
    if (!read_header_res)
        return READ_INVALID_HEADER;

    *img = create_image(header.biWidth, header.biHeight);

    uint8_t padding_sz = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        size_t read_line_res = fread(
            (img->data + (i * img->width)),
            sizeof(struct pixel),
            img->width,
            in
        );
        size_t seek_line_res = fseek(
            in,
            padding_sz,
            SEEK_CUR
        );
        if (!read_line_res || seek_line_res) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

static struct bmp_header construct_header(const struct image* img) {
    return (struct bmp_header) {
        .bfType             = BMP_FILE_TYPE,
        .bfileSize          = get_file_size(img),
        .bfReserved         = BMP_RESERVED,
        .bOffBits           = BMP_OFFSET_BITS,
        .biSize             = BMP_INFO_SIZE,
        .biWidth            = img->width,
        .biHeight           = img->height,
        .biPlanes           = BMP_PLANES,
        .biBitCount         = BMP_BITS_PER_PIXEL,
        .biCompression      = BMP_COMPRESSION,
        .biSizeImage        = BMP_SIZE_IMAGE,
        .biXPelsPerMeter    = BMP_X_PIXELS_PER_METER,
        .biYPelsPerMeter    = BMP_Y_PIXELS_PER_METER,
        .biClrUsed          = BMP_COLORS_USED,
        .biClrImportant     = BMP_COLORS_IMPORTANT
    };
}

enum write_status to_bmp(FILE* out, const struct image* img) {
    if (!is_image_valid(img))
        return WRITE_ERROR;

    struct bmp_header header = construct_header(img);

    const size_t write_header_repeats = 1;
    size_t write_header_res = fwrite(
        &header,
        sizeof(struct bmp_header),
        write_header_repeats,
        out
    );
    if (!write_header_res)
        return WRITE_ERROR;

    uint8_t padding_sz = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        size_t write_line_res = fwrite(
            img->data + i * img->width,
            sizeof(struct pixel),
            img->width,
            out
        );
        size_t seek_line_res = fseek(
            out,
            padding_sz,
            SEEK_CUR
        );
        if (write_line_res != img->width || seek_line_res == -1)
            return WRITE_ERROR;
    }

    return WRITE_OK;
}

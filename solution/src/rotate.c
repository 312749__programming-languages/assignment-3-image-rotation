#include "../include/rotate.h"

struct image rotate_counterclockwise(const struct image* img) {
    struct image rotated_img = create_image(
        img->height,
        img->width
    );

    /* Description:
     *  This part of code rotates the image 90 deg counterclockwise by transposing
     *  its pixels pseudo-matrix (represented as single-dimension array) and reversing
     *  the rows of transposed matrix.
     * */
    for (size_t i = 0; i < img->height; ++i) {
        for (size_t j = 0; j < img->width; ++j) {
            size_t src_index = i * img->width + j;
            size_t target_index = (rotated_img.width) * (j + 1) - i - 1;
            rotated_img.data[target_index] = img->data[src_index];
        }
    }

    return rotated_img;
}

#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"

#include <stdio.h>
#include <time.h>

/*
 * Is there more elegant way to store such dictionary?
 * */
#define SRC_FILE_REQUIRED       "Source filename required as 1-st argument"
#define TARGET_FILE_REQUIRED    "Target filename required as 2-nd argument"
#define TOO_MANY_ARGS           "Expected %d arguments. %d were given"
#define FILE_DOES_NOT_EXIST     "File with name %s does not exist"
#define FILE_IS_CORRUPTED       "Given file is corrupted"
#define UNABLE_TO_WRITE         "Unable to write to %s file"

int main(int argc, char** argv) {
    clock_t start = clock();

    if (argc < 3) {
        if (argc < 2)
            fprintf(stderr, SRC_FILE_REQUIRED);
        fprintf(stderr, TARGET_FILE_REQUIRED);
        return -1;
    }
    if (argc > 3) {
        fprintf(stderr, TOO_MANY_ARGS, 3, argc);
        return -1;
    }

    char* src_filename = argv[1];
    FILE* src_file = fopen(src_filename, "r");
    if (!src_file) {
        fprintf(stderr, FILE_DOES_NOT_EXIST, src_filename);
        return -1;
    }
    struct image img;
    if (from_bmp(src_file, &img) != READ_OK) {
        fprintf(stderr, FILE_IS_CORRUPTED);
        return -1;
    }

    struct image rotated_img = rotate_counterclockwise(&img);

    char* target_filename = argv[2];
    FILE* target_file = fopen(target_filename, "w");
    if (to_bmp(target_file, &rotated_img) != WRITE_OK) {
        fprintf(stderr, UNABLE_TO_WRITE, target_filename);
        return -1;
    }

    delete_image(&img);
    delete_image(&rotated_img);

    clock_t stop = clock();
    char* exec_time_fmt = "Execution time: %fs";
    double exec_time = (double) (stop - start) / CLOCKS_PER_SEC;
    printf(exec_time_fmt, exec_time);

    return 0;
}
